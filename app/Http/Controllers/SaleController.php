<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaleRequest;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class SaleController extends Controller
{
    public function sale(SaleRequest $request)
    {
        $user = User::find(auth()->id());
        $product = Product::find($request->product_id);
        $total_price = $product->price * $request->amount;

        // dd($total_price);
        $user->r_products()->attach([
            $request->product_id =>
            [
                'amount'    => $request->amount,
                'total_price' => $total_price
            ]
            ]);

        return response([
            'ok'    =>true,
            'message' => 'Transaction success',
            'data' => [
                'product' => $product,
                'amount'  => $request->amount,
                'total_price' => $total_price,
            ]
        ]);
    }

    public function mySales()
    {
        return response([
            'ok'    =>true,
            'message' => 'Transaction success',
            'data' => [
                auth()->user()->r_products
            ]
        ]);
    }
}
