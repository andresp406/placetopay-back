<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function scopeSearch(Builder $query, String $term): Builder
    {
        if($term !== '' || $term !== null) {

            return $query->where('name', 'like', "%$term%");
        }
    }
}
